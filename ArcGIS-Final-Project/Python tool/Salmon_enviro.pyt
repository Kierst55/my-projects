#import modules
import matplotlib.pyplot as plt
import arcpy
import os

##Build python-toolbox##
class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Demo Toolbox"
        self.alias = "dt"

        # List of tool classes associated with this toolbox
        self.tools = [Salmon_vs_Environmental_Factors]


class Salmon_vs_Environmental_Factors(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Salmon populations vs environmental factors"
        self.description = "Add environmental parameters to specific salmon population data"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        
        # first parameter
        pop_density = arcpy.Parameter(
            displayName = 'Salmon species population',
            name = 'population_density',
            datatype = 'GPType',
            parameterType = 'Required',
            direction = 'Input')

        # second parameter
        flow_data = arcpy.Parameter(
            displayName = 'River flow data',
            name = 'flow_data',
            datatype = 'GPType',
            parameterType = 'Required',
            direction = 'Input')

        # third parameter
        temperature_data = arcpy.Parameter(
            displayName = 'Temperature data',
            name = 'temperature_data',
            datatype = 'GPType',
            parameterType = 'Required',
            direction = 'Input')

        # fourth parameter
        dam_locations = arcpy.Parameter(
            displayName = 'Dam locations',
            name = 'dam_locations',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')
        
        #fifth parameter
        elevation_data = arcpy.Parameter(
            displayName = 'DEM',
            name = 'elevation_data',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Input')

        # sixth parameter
        watershed_boundary = arcpy.Parameter(
            displayName = 'Watershed boundary for clip',
            name = 'watershed_boundary',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Input')

        # seventh parameter
        clipped_files = arcpy.Parameter(
            displayName = 'folder destination for clipped files',
            name = 'clipped_files',
            datatype = 'DEFolder',
            parameterType = 'Required',
            direction = 'Input')
        
        # eigth parameter
        converted_tables = arcpy.Parameter(
            displayName = 'folder destination for table to table conversion from csv files',
            name = 'converted_tables',
            datatype = 'DEFolder',
            parameterType = 'Required',
            direction = 'Input')
        #ninth parameter
        plots = arcpy.Parameter(
            displayName = 'folder destination for constructed plots',
            name = 'plots',
            datatype = 'DEFolder',
            parameterType = 'Required',
            direction = 'Input')
        
        # tenth parameter
        output = arcpy.Parameter(
            displayName = 'Output',
            name = 'output',
            datatype = 'GPFeatureLayer',
            parameterType = 'Required',
            direction = 'Output')
        
        

        # return a list of parameters
        params = [pop_density, flow_data, temperature_data, 
                  dam_locations, elevation_data, watershed_boundary, 
                  clipped_files, converted_tables, plots, output]
        return params
    
    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        
        #get parameters
        pop_density = parameters[0].valueAsText
        flow_data = parameters[1].valueAsText
        temperature_data = parameters[2].valueAsText
        dam_locations = parameters[3].valueAsText
        elevation_data = parameters[4].valueAsText
        watershed_boundary = parameters[5].valueAsText
        clipped_files = parameters[6].valueAsText
        converted_tables = parameters[7].valueAsText
        plots = parameters[8].valueAsText
        output = parameters[9].valueAsText
                
        #clip data by watershed_boundary and save to designated folder
        arcpy.Clip_analysis(dam_locations, watershed_boundary, os.path.join(clipped_files, 'dam_locations_clipped.shp'))
        arcpy.Clip_analysis(elevation_data, watershed_boundary, os.path.join(clipped_files, 'elevation_data_clipped.shp'))
        
        #convert csv files to workable tables within ArcMap
        arcpy.TableToTable_conversion(pop_density, output, 'pop_density.dbf')
        arcpy.TableToTable_conversion(flow_data, converted_tables, 'flow_data.dbf')
        arcpy.TableToTable_conversion(temperature_data, converted_tables, 'temperature.dbf')
        
        #run the attribute join on clipped environmental factors to population data
        arcpy.env.workspace = converted_tables
        arcpy.JoinField_management('pop_density', 'Year', 'flow_data.dbf', 'Year')
        arcpy.JoinField_management('pop_density', 'Year', 'temperature_data.dbf', 'Date')
        arcpy.JoinField_management('pop_density', 'Year', 'dam_locations_clipped.shp', 'Year Completed')
        
        
        ##Create graphs to visualize data##
        #Topographical flow accumulation tif and flow plot png
        arcpy.env.workspace = clipped_files
        arcpy.env.overwriteOutput = True
        #Check out spatial analyst extension
        arcpy.CheckOutExtension('spatial')
        cutoff = 1000
        flow_dir = arcpy.sa.FlowDirection('elevation_data_clipped')
        accumulation = arcpy.sa.FlowAccumulation(flow_dir)
        accumulation.save(os.path.join(plots, 'topographical_flow_accumulation.tif'))
        
        values = [[accumulation.minimum, cutoff, 1], [cutoff, accumulation.maximum, 0]]
        remap = arcpy.sa.RemapRange(values)
        reclass = arcpy.sa.Reclassify(elevation_data, 'VALUE', remap)
        flow_plot_data = arcpy.RasterToNumPyArray(reclass)
        plt.imsave((os.path.join(plots, 'topographical_flow_plot.png')), flow_plot_data, cmap='gray')
        
        ##create variable for parameter data to graph
        
        #pop_density datain x, y format
        with arcpy.da.SearchCursor(in_table=pop_density, field_names=['Year', 'Spawners']) as sc:
            data = list(sc)
            pop_yr, spawners = zip(*data)
                
        #flow_data in x, y format
        with arcpy.da.SearchCursor(in_table=flow_data, field_names=['Year', 'Discharge']) as sc:
            data = list(sc)
            flow_yr, cfs = zip(*data)
        
        #temperature_data in x, y format
        with arcpy.da.SearchCursor(in_table=temperature_data, field_names=['Date', 'Temperature']) as sc:
            data = list(sc)
            temp_yr, temp_F = zip(*data)
        #generate 3 figures of parameters
        plt.figure()
        plt.plot(pop_yr, spawners, 'g-')
        plt.xlabel('Year')
        plt.ylabel('Spawner Population Size')
        plt.savefig(os.path.join(plots, 'Population_timeline.png'))
        
        plt.figure()
        plt.plot(flow_yr, cfs, 'b-')
        plt.xlabel('Year')
        plt.ylabel('Discharge (cfs)')
        plt.savefig(os.path.join(plots, 'Annual_flow.png'))
        
        plt.figure()
        plt.plot(temp_yr, temp_F, 'r-')
        plt.xlabel('Year')
        plt.ylabel('Temperature (F)')
        plt.savefig(os.path.join(plots, 'Avg_temp_timeline.png'))
        
        
        ##combine parameters to observe corelations 
        
        #salmon vs flow trends
        fig, ax1 = plt.subplots()
        
        color = 'tab:green'
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Spawner Population Size', color=color)
        ax1.plot(pop_yr, spawners, color=color)
        ax1.tick_params(axis='y', labelcolor=color)
       
        ax2 = ax1.twinx()
        color = 'tab:blue'
        ax2.set_ylabel('Discharge (cfs)', color=color)
        ax2.tick_params(axis='y', labelcolor=color)
        
        fig.tight_layout()
        plt.savefig(os.path.join(plots, 'Salmon_vs_flow_timeline.png'))
        
        #salmon vs temp trends
        fig, ax1 = plt.subplots()
        
        color = 'tab:green'
        ax1.set_xlabel('Year')
        ax1.set_ylabel('Spawner Population Size', color=color)
        ax1.plot(pop_yr, spawners, color=color)
        ax1.tick_params(axis='y', labelcolor=color)
       
        ax2 = ax1.twinx()
        color = 'tab:red'
        ax2.set_ylabel('Temperature (F)', color=color)
        ax2.tick_params(axis='y', labelcolor=color)
        
        fig.tight_layout()
        plt.savefig(os.path.join(plots, 'Salmon_vs_temperature_timeline.png'))
       
        
        messages.addMessage('Done!')
        return
