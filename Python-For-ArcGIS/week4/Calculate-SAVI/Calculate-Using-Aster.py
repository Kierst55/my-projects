#Set workspace and Input modules
import arcpy
arcpy.env.workspace = r'E:\Python\python-esri\week_4\data'
arcpy.CheckOutExtension("Spatial")
#Use Describe to look at the different bands
bands = arcpy.Describe('aster.img').children
#Create Raster objects to better identify during map-algebra 
red = arcpy.Raster(bands[1].catalogPath)
nir = arcpy.Raster(bands[2].catalogPath)
#Compute SAVI
SAVI = arcpy.sa.Float((1 + 0.5) * (nir - red)) / (nir + red + 0.5)
#Save the SAVI raster and build pyramid layers
SAVI.save('SAVI.tif')
arcpy.BuildPyramids_management('SAVI.tif', resample_technique='BILINEAR')