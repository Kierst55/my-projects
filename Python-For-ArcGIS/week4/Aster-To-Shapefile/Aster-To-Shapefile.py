#Setup imports and workspace
import arcpy 
arcpy.env.workspace = r'E:\Python\python-esri\week_4\data'
arcpy.CheckOutExtension("Spatial")
#Run Extract Multi Values to Points tool within the Extraction Toolset
arcpy.sa.ExtractMultiValuesToPoints("sites.shp", "aster.img")