#Set workspace and Input modules
import arcpy
import os
arcpy.env.workspace = r'E:\Python\python-esri\week_4\data'
arcpy.CheckOutExtension("Spatial")
#Create a weighted neighborhood from the file
fn = os.path.join(arcpy.env.workspace, '3X3.txt')
nbrhood = arcpy.sa.NbrWeight(fn)
#Sharpen the raster using the weighted neighborhood and sum
sharpened = arcpy.sa.FocalStatistics('aster.img', nbrhood, 'sum')
#Save raster and build pyramids 
sharpened.save('sharpened.tif')
arcpy.BuildPyramids_management('sharpened.tif', resample_technique='BILINEAR')