import arcpy
arcpy.env.workspace = r'E:\Python\python-esri\week_2\data'
arcpy.MakeFeatureLayer_management('deer.shp', 'deer_layer')
arcpy.MakeFeatureLayer_management('deer.shp', 'deer_layer2')
arcpy.SelectLayerByAttribute_management('deer_layer2', "NEW_SELECTION", '"Id" = 375')
arcpy.SelectLayerByLocation_management('deer_layer', 'WITHIN_A_DISTANCE', 'deer_layer2', '200', 'NEW_SELECTION')
arcpy.GetCount_management('deer_layer')
print arcpy.GetCount_management('deer_layer')