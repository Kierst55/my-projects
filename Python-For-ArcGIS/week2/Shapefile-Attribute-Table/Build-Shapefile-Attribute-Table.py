import arcpy
arcpy.env.workspace = r'E:\Python\python-esri\week_2\data'
shp_list = arcpy.ListFeatureClasses(feature_type='Point')
for i in shp_list:
    srs = arcpy.Describe(i).spatialReference
    spatial_name = srs.name
    if spatial_name == 'NAD_1983_UTM_Zone_12N':
        arcpy.AddXY_management(i)
        print i, ':', srs.name
