import arcpy
arcpy.env.workspace = r'E:\Python\python-esri\week_2\data'
arcpy.MakeFeatureLayer_management('Municipalities.shp', 'Muni_layer_prob2')
arcpy.MakeFeatureLayer_management('county.shp', 'county_layer_prob2')
arcpy.AddJoin_management('Muni_layer_prob2', 'COUNTYNBR', 'county_layer_prob2', 'COUNTYNBR')
stats_vt = arcpy.ValueTable(2)
stats_vt.addRow('Municipalities.POPLASTCEN MIN')
stats_vt.addRow('Municipalities.POPLASTCEN MAX')
stats_vt.addRow('Municipalities.POPLASTCEN MEAN')
stats_vt.addRow('Municipalities.POPLASTCEN STD')
arcpy.Statistics_analysis('Muni_layer_prob2', 'E:\python\python-esri\week_2\data\stats_prob2.dbf', stats_vt, 'county.name')
