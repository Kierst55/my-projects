#Import modules needed
import matplotlib.pyplot as plt
import numpy as np
import arcpy
arcpy.env.overwriteOutput = True
#Check out Spatial Analyst extension
arcpy.CheckOutExtension('spatial')
#Create variables for needed parameters
elevation_rast = 'D:\Python\python-esri\week_5\data\cach_ned10_clip_int.img'
flow_accum = r'D:\Python\python-esri\week_5\data\flow_accum.tif'
flow_plot = r'D:\Python\python-esri\week_5\data\flow_plot.png'
cutoff = 1000
#Calculate flow direction and accumulation
direction = arcpy.sa.FlowDirection(elevation_rast)
accumulation = arcpy.sa.FlowAccumulation(direction)
#Save accumulation as output for parameter 2
accumulation.save(flow_accum)
#Create a Remap Range
values = [[accumulation.minimum, cutoff, 1], [cutoff, accumulation.maximum, 0]]
remap = arcpy.sa.RemapRange(values)
#Reclassify values in raster
reclass = arcpy.sa.Reclassify(elevation_rast, 'VALUE', remap)
#Create Numpy Array of results
data = arcpy.RasterToNumPyArray(reclass)
#Save as a png
plt.imsave(flow_plot, data, cmap='gray')