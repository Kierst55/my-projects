import arcpy
import csv
#Combined search cursor and output csv file
with arcpy.da.SearchCursor(in_table=r'E:\Python\python-esri\week_3\data\Municipalities.shp', field_names=['NAME', 'POPLASTCEN', 'POPLASTEST'], where_clause='"COUNTYSEAT"=1') as searcher, open('myfile.csv', 'wb') as fp:
    #Create a writer and write out column names
    data_writer = csv.writer(fp)
    data_writer.writerow(['NAME', 'POPLASTCEN', 'POPLASTEST'])
    #Write the data to the csv file by looping through the rows of the Search cursor
    for row in searcher:
        data_writer.writerow(row)