import arcpy
import csv
arcpy.env.overwriteOutput = True
srs = arcpy.SpatialReference('WGS 1984 UTM Zone 12N')
arcpy.env.workspace = r'E:\Python\python-esri\week_3\data'
arcpy.CreateFeatureclass_management(out_path=arcpy.env.workspace, out_name='deer_points.shp', geometry_type='POINT', spatial_reference=srs)
arcpy.AddField_management('deer_points.shp', 'DATE', 'LONG')
with arcpy.da.InsertCursor('deer_points.shp', ['id', 'DATE', 'SHAPE@X', 'SHAPE@Y']) as inserter, open(r'E:\Python\python-esri\week_3\data\track2.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        list_float = map(float, row)
        inserter.insertRow(list_float)
del inserter