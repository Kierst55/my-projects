import arcpy
import csv
arcpy.env.overwriteOutput = True
arcpy.env.workspace = r'E:\Python\python-esri\week_3\data'
srs_input = arcpy.SpatialReference('WGS 1984 UTM Zone 12N')
srs_output = arcpy.SpatialReference('WGS 1984')
arcpy.CreateFeatureclass_management(out_path=arcpy.env.workspace, out_name='deer_polyline.shp', geometry_type='POLYLINE', spatial_reference=srs_output)
with open(r'E:\Python\python-esri\week_3\data\track2.csv', 'rb') as fp:
    pts = arcpy.Array()
    reader = csv.reader(fp)
    next(reader)
    for line in reader:
        row = map(float, line)
        pts.add(arcpy.Point(row[2], row[3]))
line = arcpy.Polyline(pts, srs_input)
with arcpy.da.InsertCursor('deer_polyline.shp', ['shape@']) as inserter:
    inserter.insertRow([line])